import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    delete(city) {
      if (confirm('Are you sure you want to delete this city?')) {
        this.sendAction('destroyCity', city);
      }
    },
    save3(params) {
      console.log("save3 city-detail.js");
      console.log(params);
      this.sendAction('save3', params);
    }

  }
});
